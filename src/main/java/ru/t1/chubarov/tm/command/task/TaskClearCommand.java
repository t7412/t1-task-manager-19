package ru.t1.chubarov.tm.command.task;

import ru.t1.chubarov.tm.exception.AbstractException;
import ru.t1.chubarov.tm.util.TerminalUtil;

public final class TaskClearCommand extends AbstractTaskCommand {

    @Override
    public void execute() throws AbstractException {
        System.out.println("[TASK CLEAR]");
        getTaskService().clear();
    }

    @Override
    public String getName() { return "task-clear"; }

    @Override
    public String getDescription() { return "Remove all task."; }

}
