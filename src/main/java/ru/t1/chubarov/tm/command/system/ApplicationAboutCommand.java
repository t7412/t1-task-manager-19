package ru.t1.chubarov.tm.command.system;

public final class ApplicationAboutCommand extends AbstractSystemCommand{

    @Override
    public void execute() {
        System.out.println("[ABOUT]");
        System.out.println("name: Evgeni Chubarov");
        System.out.println("e-mail: echubarov@t1-consulting.ru");
    }

    @Override
    public String getName() {
        return "about";
    }

    @Override
    public String getDescription() {
        return "Show about program.";
    }

    @Override
    public String getArgument() { return "-a"; }

}
