package ru.t1.chubarov.tm.command.project;

import ru.t1.chubarov.tm.exception.AbstractException;
import ru.t1.chubarov.tm.exception.entity.ProjectNotFoundException;
import ru.t1.chubarov.tm.exception.field.ProjectIdEmptyException;
import ru.t1.chubarov.tm.exception.field.TaskIdEmptyException;
import ru.t1.chubarov.tm.model.Task;
import ru.t1.chubarov.tm.util.TerminalUtil;

public final class ProjectTaskUnbindToProjectCommand extends AbstractProjectCommand {

    @Override
    public void execute() throws AbstractException {
        System.out.println("[UNBIND TASK FROM PROJECT");
        System.out.println("ENTER PROJECT ID: ");
        final String projectId = TerminalUtil.nextLine();
        System.out.println("ENTER TASK ID: ");
        final String taskId = TerminalUtil.nextLine();
        getProjectTaskService().unbindTaskFromProject(projectId, taskId);
    }

    @Override
    public String getName() {
        return "task-unbind-from-project";
    }

    @Override
    public String getDescription() {
        return "Unbind task from project bi id.";
    }

}
