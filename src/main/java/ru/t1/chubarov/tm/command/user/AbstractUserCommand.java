package ru.t1.chubarov.tm.command.user;

import ru.t1.chubarov.tm.api.service.IUserService;
import ru.t1.chubarov.tm.command.AbstractCommand;
import ru.t1.chubarov.tm.enumerated.Status;
import ru.t1.chubarov.tm.model.User;


import java.util.List;

public abstract class AbstractUserCommand extends AbstractCommand {

    protected IUserService getUserService() { return getServiceLocator().getUserService(); }

    public String getArgument() { return null; }
//
//    public void renderTask(final List<Task> tasks) {
//        System.out.println("[TASK LIST]");
//        int index = 1;
//        for (final Task task : tasks) {
//            if (task == null) return;
//            System.out.println(index + ". " + task.getName());
//            System.out.println("    id: "+task.getId());
//            System.out.println("    status: "+task.getStatus());
//            System.out.println("    desc: "+task.getDescription());
//            index++;
//        }
//    }
//
    public void showUser(final User user) {
        if (user == null) return;
        System.out.println("ID: " + user.getId());
        System.out.println("  LOGIN: " + user.getLogin());
        System.out.println("  EMAIL: " + user.getEmail());
        System.out.println("  FIRST NAME: " + user.getFirstName());
        System.out.println("  LAST NAME: " + user.getLastName());
        System.out.println("  MIDDLE NAME: " + user.getMiddleName());
    }

}
