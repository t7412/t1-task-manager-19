package ru.t1.chubarov.tm.command.user;

import ru.t1.chubarov.tm.exception.AbstractException;
import ru.t1.chubarov.tm.model.User;
import ru.t1.chubarov.tm.util.TerminalUtil;

public final class UserViewProfileCommand extends AbstractUserCommand {

    private final String NAME = "view-user-profile";
    private final String DESCRIPTION = "View profile of current user.";

    @Override
    public void execute() throws AbstractException {
        final User user = serviceLocator.getAuthService().getUser();
        System.out.println("[VIEW USER PROFILE]");
        System.out.println("ID: "+user.getId());
        System.out.println("LOGIN: "+user.getLogin());
        System.out.println("FIRST NAME: "+user.getFirstName());
        System.out.println("LAST NAME: "+user.getLastName());
        System.out.println("MIDDLE NAME: "+user.getMiddleName());
        System.out.println("EMAIL: "+user.getEmail());
        System.out.println("ROLE: "+user.getRole());
    }

    @Override
    public String getName() { return NAME; }

    @Override
    public String getDescription() { return DESCRIPTION; }

}
