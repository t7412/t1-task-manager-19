package ru.t1.chubarov.tm.command.task;

import ru.t1.chubarov.tm.api.service.IProjectTaskService;
import ru.t1.chubarov.tm.api.service.ITaskService;
import ru.t1.chubarov.tm.command.AbstractCommand;
import ru.t1.chubarov.tm.enumerated.Status;
import ru.t1.chubarov.tm.model.Task;

import java.util.List;

public abstract class AbstractTaskCommand extends AbstractCommand {

    protected ITaskService getTaskService() { return getServiceLocator().getTaskService(); }
    protected IProjectTaskService getProjectTaskService() { return getServiceLocator().getProjectTaskService(); }

    public String getArgument() { return null; }

    public void renderTask(final List<Task> tasks) {
        System.out.println("[TASK LIST]");
        int index = 1;
        for (final Task task : tasks) {
            if (task == null) return;
            System.out.println(index + ". " + task.getName());
            System.out.println("    id: "+task.getId());
            System.out.println("    status: "+task.getStatus());
            System.out.println("    desc: "+task.getDescription());
            index++;
        }
    }

    public void showTask(final Task task) {
        if (task == null) return;
        System.out.println("ID: " + task.getId());
        System.out.println("NAME: " + task.getName());
        System.out.println("DESCRIPTION: " + task.getDescription());
        System.out.println("STATUS: " + Status.toName(task.getStatus()));
    }

}
