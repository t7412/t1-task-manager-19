package ru.t1.chubarov.tm.service;

import ru.t1.chubarov.tm.api.repository.IProjectRepository;
import ru.t1.chubarov.tm.api.service.IProjectService;
import ru.t1.chubarov.tm.enumerated.Status;
import ru.t1.chubarov.tm.exception.AbstractException;
import ru.t1.chubarov.tm.exception.field.*;
import ru.t1.chubarov.tm.model.Project;

public final class ProjectService extends AbstractService<Project, IProjectRepository> implements IProjectService {

    public ProjectService(IProjectRepository repository) {
        super(repository);
    }

    @Override
    public Project create(final String name, final String description) throws AbstractException {
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null) throw new DescriptionEmptyException();
        final Project project = new Project(name, Status.NOT_STARTED);
        project.setName(name);
        project.setDescription(description);
        add(project);
        return project;
    }

    @Override
    public Project updateById(final String id, final String name, final String description) throws AbstractException {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        final Project project = findOneById(id);
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @Override
    public Project updateByIndex(final Integer index, final String name, final String description) throws AbstractException{
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        final Project project = findOneByIndex(index);
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @Override
    public Project removeByIndex(final Integer index) throws IndexIncorrectException {
        if (index == null || index < 0) throw new IndexIncorrectException();
        return repository.removeByIndex(index);
    }

    @Override
    public Project changeProjectStatusByIndex(final Integer index, final Status status) throws AbstractException {
        if (index == null || index <0) throw new IndexIncorrectException();
        if (index >= getSize()) throw new IndexIncorrectException();
        final Project project = findOneByIndex(index);
        project.setStatus(status);
        return project;
    }

    @Override
    public Project changeProjectStatusById(final String id, final Status status) throws AbstractException {
        if (id == null || id.isEmpty()) return null;
        final Project project = findOneById(id);
        project.setStatus(status);
        return project;
    }

}
