package ru.t1.chubarov.tm;

import ru.t1.chubarov.tm.component.Bootstrap;
import ru.t1.chubarov.tm.exception.AbstractException;

public final class Application {

    public static void main(final String[] args) throws AbstractException {
        final Bootstrap bootstrap = new Bootstrap();
        bootstrap.start(args);
    }

}
