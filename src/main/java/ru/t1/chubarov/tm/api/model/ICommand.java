package ru.t1.chubarov.tm.api.model;

import ru.t1.chubarov.tm.exception.AbstractException;

public interface ICommand {

    String getName();

    String getArgument();

    String getDescription();

    void execute() throws AbstractException;

}
