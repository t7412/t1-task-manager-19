package ru.t1.chubarov.tm.api.repository;

import ru.t1.chubarov.tm.exception.field.IndexIncorrectException;
import ru.t1.chubarov.tm.model.Task;
import java.util.List;

public interface ITaskRepository extends IRepository<Task> {

    Task removeByIndex(Integer index) throws IndexIncorrectException;

    List<Task> findAllByProjectId(String projectId);

}
