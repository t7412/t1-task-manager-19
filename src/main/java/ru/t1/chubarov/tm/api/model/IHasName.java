package ru.t1.chubarov.tm.api.model;

public interface IHasName {

    String getName();

    void setName(String name);

}
