package ru.t1.chubarov.tm.api.service;

import ru.t1.chubarov.tm.enumerated.Status;
import ru.t1.chubarov.tm.exception.AbstractException;
import ru.t1.chubarov.tm.exception.field.IndexIncorrectException;
import ru.t1.chubarov.tm.model.Project;


public interface IProjectService extends IService<Project> {

    Project create (String name, String description) throws AbstractException;

    Project updateById(String id, String name, String description) throws AbstractException;

    Project updateByIndex(Integer index, String name, String description) throws AbstractException;

    Project removeByIndex(Integer index) throws IndexIncorrectException;

    Project changeProjectStatusByIndex(Integer index, Status status) throws AbstractException;

    Project changeProjectStatusById(String id, Status status) throws AbstractException;

}
