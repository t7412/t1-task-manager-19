package ru.t1.chubarov.tm.api.controller;

import ru.t1.chubarov.tm.exception.AbstractException;

public interface IProjectController {

    void createProject() throws AbstractException;

    void showProjects();

    void clearProjects();

    void showProjectByIndex() throws AbstractException;

    void showProjectById() throws AbstractException;

    void updateProjectByIndex() throws AbstractException;

    void updateProjectById() throws AbstractException;

    void removeProjectByIndex() throws AbstractException;

    void removeProjectById() throws AbstractException;

    void changeProjectStatusByIndex() throws AbstractException;

    void changeProjectStatusById() throws AbstractException;

    void startProjectByIndex() throws AbstractException;

    void startProjectById() throws AbstractException;

    void completeProjectByIndex() throws AbstractException;

    void completeProjectById() throws AbstractException;

}
