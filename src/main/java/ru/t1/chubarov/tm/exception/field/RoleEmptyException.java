package ru.t1.chubarov.tm.exception.field;

public final class RoleEmptyException extends AbstractFieldException {

    public RoleEmptyException() {
        super("Error. Role is empty.");
    }

}
